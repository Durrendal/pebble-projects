#include <pebble.h>
#include <src/c/Vostok.h>

static Window *s_main_window;
static TextLayer *s_time_layer, *s_logo_layer, *s_date_layer, *s_day_layer;
static Layer *s_canvas_layer; //s_hands_layer;

//static GPath *s_tick_paths[NUM_CLOCK_TICKS];
static GPath *s_minute_arrow, *s_hour_arrow;

static void canvas_update_proc(Layer *layer, GContext *ctx) {
  // Draw Stuff
  graphics_context_set_stroke_color(ctx, GColorWhite);
  graphics_context_set_fill_color(ctx, GColorCobaltBlue);
  graphics_context_set_stroke_width(ctx, 10);

  // Outer circle
  GPoint center = GPoint(90, 90);
  uint16_t radius = 90;

  graphics_draw_circle(ctx, center, radius);
  graphics_fill_circle(ctx, center, radius);

  graphics_context_set_stroke_width(ctx, 3);

  // Inner Circle
  GPoint center2 = GPoint(90, 90);
  uint16_t radius2 = 65;

  graphics_draw_circle(ctx, center2, radius2);

  // Vert/Horz intersect
  GPoint startv = GPoint(90, -180);
  GPoint endv = GPoint(90, 180);
  GPoint starth = GPoint(-180, 90);
  GPoint endh = GPoint(180, 90);

  graphics_draw_line(ctx, startv, endv);
  graphics_draw_line(ctx, starth, endh);

  // Radial Points
  graphics_context_set_stroke_color(ctx, GColorDarkGray);
  graphics_context_set_fill_color(ctx, GColorDarkGray);
  graphics_context_set_stroke_width(ctx, 6);

  //GRect rect_bounds = GRect(130, 130, 40, 60);
  // graphics_draw_rect(ctx, rect_bounds);

  //#define TOP_RIGHT 72
  //#define BOT_RIGHT 240
  //#define BOT_LEFT  384
  //#define TOP_LEFT 552
  
  //GPoint start11 = GPoint(45, 0);
  //GPoint end_11 = GPoint(90, 90 );
  //GPoint start10 = GPoint(-90, 0);
  //GPoint end_10 = GPoint(90, 90);
  //GPoint start8 = GPoint(0, 135);
  //GPoint end_8 = GPoint(90, 90);
  //GPoint start7 = GPoint(0, 225);
  //GPoint end_7 = GPoint(90, 90);

  // These won't draw correctly, everything else points back to 90
  //GPoint start4 = GPoint(0, 120);
  //GPoint end_4 = GPoint(90, 90);
  //GPoint start5 = GPoint(0, 120);
  //GPoint end_5 = GPoint(90, 90);

  //GPoint start1 = GPoint(135, 0);
  //GPoint end_1 = GPoint(90, 90);
  //GPoint start2 = GPoint(225, 0);
  //GPoint end_2 = GPoint(90, 90);
  
  //graphics_draw_line(ctx, start11, end_11);
  //graphics_draw_line(ctx, start10, end_10);
  //graphics_draw_line(ctx, start8, end_8);
  //graphics_draw_line(ctx, start7, end_7);
  //graphics_draw_line(ctx, start5, end_5);
  //graphics_draw_line(ctx, start4, end_4);
  //graphics_draw_line(ctx, start2, end_2);
  //graphics_draw_line(ctx, start1, end_1);
}

static void main_window_load(Window *window) {
  // Get information about the Window
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_canvas_layer = layer_create(
				GRect(0, 0, 180, 180));
  
  // Create the TextLayer with specific bounds
  s_time_layer = text_layer_create(
				   GRect(15, 55, 90, 50));
  s_date_layer = text_layer_create(
				    GRect(72, 90, 90, 50));
  s_day_layer = text_layer_create(
  				  GRect(64, 115, 90, 50));

  // Improve the layout to be more like a watchface
  text_layer_set_background_color(s_time_layer, GColorClear);
  text_layer_set_text_color(s_time_layer, GColorLightGray);
  // text_layer_set_text(s_time_layer, "00:00");
  text_layer_set_font(s_time_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD));
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);

  text_layer_set_background_color(s_date_layer, GColorClear);
  text_layer_set_text_color(s_date_layer, GColorLightGray);
  //text_layer_set_text(s_date_layer, "Feb");
  text_layer_set_font(s_date_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(s_date_layer, GTextAlignmentCenter);

    text_layer_set_background_color(s_day_layer, GColorClear);
    text_layer_set_text_color(s_day_layer, GColorLightGray);
    text_layer_set_text(s_day_layer, "Mon");
    text_layer_set_font(s_day_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    text_layer_set_text_alignment(s_day_layer, GTextAlignmentCenter);
  
  s_logo_layer = text_layer_create(
				   GRect(0, 152, 140, 90));
  
  text_layer_set_background_color(s_logo_layer, GColorClear);
  text_layer_set_text_color(s_logo_layer, GColorWhite);
  text_layer_set_text(s_logo_layer, "@ CCCP");
  text_layer_set_font(s_logo_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
  text_layer_set_text_alignment(s_logo_layer, GTextAlignmentRight);

  layer_set_update_proc(s_canvas_layer, canvas_update_proc);

  // Add it as a child layer to the Window's root layer
  layer_add_child(window_get_root_layer(window), s_canvas_layer);
  layer_add_child(window_layer, text_layer_get_layer(s_time_layer));
  layer_add_child(window_layer, text_layer_get_layer(s_logo_layer));
  layer_add_child(window_layer, text_layer_get_layer(s_date_layer));
  layer_add_child(window_layer, text_layer_get_layer(s_day_layer));

  layer_mark_dirty(s_canvas_layer);
}

static void main_window_unload(Window *window) {
  window_destroy(s_main_window);
  text_layer_destroy(s_time_layer);
}


static void unified_tick_handler(struct tm *tick_time, TimeUnits units_changed) {

  static char s_time_text[] = "00:00";
  static char s_date_text[] = "XXX 00";
  static char s_day_text[] = "XXX";

  char *time_format = clock_is_24h_style() ? "%R" : "%I:%M";
  strftime(s_time_text, sizeof(s_time_text), time_format, tick_time);

  if (!clock_is_24h_style() && (s_time_text[0] == '0')) {
    memmove(s_time_text, &s_time_text[1], sizeof(s_time_text) -1);
  }

  text_layer_set_text(s_time_layer, s_time_text);

  if (units_changed & DAY_UNIT) {
    strftime(s_date_text, sizeof(s_date_text), "%b %e", tick_time);
    text_layer_set_text(s_date_layer, s_date_text);
    
    strftime(s_day_text, sizeof(s_day_text), "%a", tick_time);
    text_layer_set_text(s_day_layer, s_day_text);
  }
}

static void init() {
  s_main_window = window_create();

  window_set_window_handlers(s_main_window, (WindowHandlers) {
      .load = main_window_load,
	.unload = main_window_unload
	});
	
  window_stack_push(s_main_window, true);
  
  tick_timer_service_subscribe(MINUTE_UNIT, unified_tick_handler);

  time_t now = time(NULL);
  struct tm *t = localtime(&now);
  unified_tick_handler(t, DAY_UNIT);
}

static void deinit() {
 window_destroy(s_main_window);

}

int main() {
  init();
  app_event_loop();
}
